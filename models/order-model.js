const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: true
    },
    orderDate: {
        type: Date,
        require: true
    },
    shipDate: {
        type: Date,
        require: true
    },
    receiverAddress: {
        type: String,
        required: true
    },
    note: {
        type: String
    },
    provisionalSum: {
        type: String
    },
    total: {
        type: String
    },
    discount: {
        type: String
    },
    promoUsed: {
        type: Schema.Types.ObjectId,
        ref: 'Promo'
    },
    orderDetails: [
        {
            type: Schema.Types.ObjectId,
            ref: 'OrderDetail',
            //require: true
        }
    ],
    isDeleted: {
        type: Boolean,
        default: false
    }
})

const Order = mongoose.model('Order', orderSchema)
module.exports = Order