const mongoose = require('mongoose')
const Schema = mongoose.Schema

const orderDetailSchema = new Schema({
    product: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    },
    unitPrice: {
        type: Number,
        require: true
    },
    quantity: {
        type: Number,
        require: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    order: {
        type: Schema.Types.ObjectId,
        ref: 'Order',
        require: true
    }
})

const OrderDetail = mongoose.model('OrderDetail', orderDetailSchema)
module.exports = OrderDetail