const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productSchema = new Schema({
    productName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    urlImage: {
        type: String,
        required: true
    },
    productType: {
        type: Schema.Types.ObjectId,
        ref: 'ProductType',
        required: true
    },
    orderDetails: [{
        type: Schema.Types.ObjectId,
        ref: 'OrderDetail',
    }],
    isDeleted: {
        type: Boolean,
        default: false
    }
})

const Product = mongoose.model('Product', productSchema)
module.exports = Product