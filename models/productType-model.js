const mongoose = require('mongoose')
const Schema = mongoose.Schema

const productType = new Schema({
    typeName: {
        type: String,
        required: true
    },
    products: [{
        type: Schema.Types.ObjectId,
        ref: 'Product'
    }],
    isDeleted: {
        type: Boolean,
        default: false
    }
})

const ProductType = mongoose.model('ProductType', productType)
module.exports = ProductType