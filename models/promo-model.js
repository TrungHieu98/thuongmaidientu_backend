const mongoose = require('mongoose')
const Schema = mongoose.Schema

const promoSchema = new Schema({
    promoName: {
        type: String,
        required: true
    },
    percent: {
        type: String,
        require: true
    },
    maxDiscount: {
        type: Number,
        required: true
    },
    minOrderPrice: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    producer: {
        type: String,
        required: true
    },
    expiredDate: {
        type: Date,
        required: true
    },
    urlImage: {
        type: String,
        required: true
    },
    users: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
    }],
    orders: [{
        type: Schema.Types.ObjectId,
        ref: 'Promo',
    }],
    isDeleted: {
        type: Boolean,
        default: false
    }
})

const Promo = mongoose.model('Promo', promoSchema)
module.exports = Promo