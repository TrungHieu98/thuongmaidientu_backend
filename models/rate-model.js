const mongoose = require('mongoose')
const Schema = mongoose.Schema

const rateSchema = new Schema({
    star: {
        type: Number,
        required: true
    },
    comment: {
        type: String,
    },
    products: [{
        type: Schema.Types.ObjectId,
        ref: 'Product'
    }],
    isDeleted: {
        type: Boolean,
        default: false
    }
})

const Rate = mongoose.model('Rate', rateSchema)
module.exports = Rate