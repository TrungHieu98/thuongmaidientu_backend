const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
    userName: {
        type: String,
        required: true
    },
    birthday: {
        type: Date,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        required: true
    },
    priority: {
        type: Number,
        default: 0
    },
    orders: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Order'
        }
    ],
    promos: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Promo'
        }
    ],
    isDeleted: {
        type: Boolean,
        default: false
    }
})

const User = mongoose.model('User', userSchema)
module.exports = User