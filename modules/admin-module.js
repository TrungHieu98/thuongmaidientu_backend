const express = require('express')
const router = express.Router()
const User = require('../models/user-model')

//add a admin
router.post('/add', (req, res) => {
    const { email } = req.body
    User.findOne({ email: email })
        .then(user => {
            user.priority = 1
            user.save()
                .then(
                    res.status(200).json(user)
                )
                .catch(
                    res.status(400).json({ msg: 'Hệ thống gặp lỗi, hãy thử lại sau' })
                )
        })
        .catch(err => {
            res.status(400).json({ msg: 'Người dùng không tồn tại' })
        })
})

// router.post('/add', (req, res) => {
//     User.findOne({ email: req.body.email })
//         .then(user => {
//             if (user !== null) {

//                 user.priority = 1;

//                 user.save()
//                     .then(
//                         res.status(200).json(user)
//                     )
//                     .catch(err => {
//                         res.status(400).json({ msg: 'Hệ thống gặp lỗi khi lưu thông tin' })
//                     })
//             }
//             else {
//                 res.status(400).json({ msg: 'Người dùng không tồn tại !' })
//             }
//         })
//         .catch(err => {
//             res.status(400).json({ msg: 'Hệ thống gặp lỗi khi thêm admin' })
//         })
// })

//get list admin
router.get("/", (req, res) => {
    User.find({ isDeleted: false, priority: 1 })
        .then(admins => {
            res.status(200).json(admins)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Không tìm được dữ liệu' })
        })
})

router.post('/delete/:id', (req, res) => {
    const admin_id = req.params.id
    console.log(req.params.id)
    User.findById(admin_id)
        .then(admin => {
            admin.priority = 0
            admin.save()
                .then(admin => res.status(200).json(admin))
                .catch(err => res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau', err }))
        })
        .catch(err => {
            res.status(400).json({ msg: 'Không tìm được dữ liệu', err })
        })
})

module.exports = router;