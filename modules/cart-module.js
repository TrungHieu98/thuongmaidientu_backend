const express = require('express')
const router = express.Router()

const Product = require('../models/product-model')

//api nhận vào danh sách id, sau đó tìm product chứa id tương ứng cho vào orderDetail
router.post('/getorderdetail', async function (req, res) {
    const { items } = req.body
    const promises = []

    Object.keys(items).map(key => {
        const p = Promise.resolve(
            getProduct(items[key])
                .then(product => {
                    const orderDetail = {
                        product: product,
                        quantity: items[key].quantity,
                        unitPrice: product.price,
                        order: null
                    }
                    return orderDetail
                })
        )
        promises.push(p)
    })
    Promise.all(promises)
        .then(result => res.status(200).json(result))
        .catch(err => res.status(400).json({ msg: 'Không tìm được dữ liệu' }))
})

const getProduct = (item) => {
    return new Promise(resolve => {
        Product.findById(item, (err, product) => {
            if (err) console.log('loi')
            resolve(product)
        })
    })
}

module.exports = router;