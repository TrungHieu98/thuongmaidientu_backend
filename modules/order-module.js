const express = require('express')
const router = express.Router()

// const User = require('../models/user-model')
const Order = require('../models/order-model')
const User = require('../models/user-model')
// const bson = require('bson-objectid')
const OrderDetails = require('../models/orderDetail-model')
const Product = require('../models/product-model')

//get user by id and list order embed
router.get("/:id", (req, res) => {
    Order.findById(req.params.id, (err, order) => {
        if (err) return res.status(400).json({ msg: 'Loi tim kiem' })
        User.findById(order.user, (err, user) => {
            if (err) return res.status(400).json({ msg: 'Loi tim kiem' })
            return res.status(200).json({ order, user })
        })
    })
})

router.get("/", (req, res) => {
    Order.find({})
        .then(async function (orders) {
            orders = await getUserForEachOrder(orders)
            console.log('orders', orders)
            res.status(200).json(orders)
        })

        .catch(err => res.status(400).json('sai'))
})


const getUserForEachOrder = (orders) => {
    return new Promise((resolve, reject) => {
        let promises = []
        orders.map(order => {
            const p = new Promise(async function (resolve) {
                order.user = await User.findById(order.user)
                // console.log(order)
                resolve(order)
            })
            promises.push(p)
        })
        Promise.all(promises)
            .then(result => {
                console.log('arr', result)
                resolve(result)
            })
            .catch(err => reject(err))
    })
}

// router.get("/", (req, res) => {
//     Order.find({ isDeleted: false }, (err, order) => {
//         if (err)
//             res.status(400).json({ msg: 'Không tìm được dữ liệu' })
//         res.status(200).json(order)
//     })
// })


router.post('/create', (req, res) => {
    const { user, orderDate, shipDate, receiverAddress, note, orderDetails, _id, promoUsed, provisionalSum, total, discount } = req.body
    const newOrder = new Order({
        _id: _id,
        user: user,
        orderDate: orderDate,
        shipDate: shipDate,
        receiverAddress: receiverAddress,
        note: note,
        orderDetails: orderDetails,
        promoUsed: promoUsed,
        provisionalSum: provisionalSum,
        total: total,
        discount: discount
    })
    newOrder.save()
        .then(order => {
            res.status(200).json(order)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau', err })
        })
})

router.put("/update/:id", (req, res) => {
    Order.findById(req.params.id, (err, order) => {

        if (req.body.orderDate !== undefined)
            order.orderDate = req.body.orderDate

        if (req.body.shipDate !== undefined)
            order.shipDate = req.body.shipDate

        order.save()
            .then(order => {
                res.json('object updated successfully: ' + order)
            })
            .catch(err => {
                res.status(400).send("unable to update data: " + err)
            })
    })
})

router.put("/update/:id", (req, res) => {

    const newData = {
        //typeName: req.body.typeName
    }

    Order.findByIdAndUpdate(req.params.id, newData)
        .then(order => {
            res.status(200).json(order)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Cập nhật thất bại !' })
        })
});

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    Order.findByIdAndUpdate(req.params.id, { $set: { isDeleted: true } })
        .then(order => {
            res.status(200).json({ msg: 'Đã xóa !' })
        })
        .catch(err => {
            res.status(400).json({ msg: 'Xóa thất bại !' })
        })
})

router.post('/getorderdetails', (req, res) => {
    const { order_id } = req.body
    Order.findById(order_id)
        .then(order => {
            const arrOrderDetails = order.orderDetails.map(orderDetail_id => {
                return OrderDetails.findById(orderDetail_id)
                    .then(async orderDetail => {
                        orderDetail.product = await Product.findById(orderDetail.product)
                        return orderDetail
                    })
            })
            Promise.all(arrOrderDetails)
                .then(orderDetails => {
                    res.status(200).json(orderDetails)
                })
                .catch(err => {
                    res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau', err })
                })
        })
        .catch(err => {
            res.status(400).json({ msg: 'Không tìm thấy dữ liệu', err })
        })
})

module.exports = router;