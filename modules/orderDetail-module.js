const express = require('express')
const router = express.Router()

const OrderDetail = require('../models/orderDetail-model')
const Product = require('../models/product-model')

//get orderDetail by id and list product embed
router.get("/:id", (req, res) => {

    let arrProduct = []
    // console.log(req.params.id)
    OrderDetail.findById(req.params.id, (err, orderDetail) => {
        if (err || orderDetail.isDeleted) throw err
        //return res.status(200).json({ orderDetail })
        const products = orderDetail.get('products')
        if (products.length > 0)
            return products.map(id => {

                return Product.findById(id, (err, product) => {

                    if (err) throw err
                    arrProduct = [...arrProduct, product]

                })
                    .then(() => {
                        return res.status(200).json({ orderDetail, arrProduct })
                    })
            })
        else
            return res.status(200).json({ orderDetail })
    })
})

router.get('/', (req, res) => {
    OrderDetail.find({}, (err, orderDetail) => {
        if (err || orderDetail.isDeleted) throw err
        return res.json(orderDetail)
    })
})

router.post('/create', (req, res) => {
    const { product, unitPrice, quantity, order } = req.body
    const orderDetail = new OrderDetail({
        product: product,
        unitPrice: unitPrice,
        quantity: quantity,
        order: order
    })
    orderDetail.save()
        .then(orderDetail => {
            res.status(200).json(orderDetail)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi khi lưu thông tin', err })
        })
})

// router.post('/createincart', (req, res) => {
//     const promises = []
//     const { orderDetailsInCart, order } = req.body
//     console.log(req.body)
//     orderDetailsInCart.forEach(orderDetail => {
//         const p = Promise.resolve(() => {
//             const { product, unitPrice, quantity } = orderDetail
//             const newOrderDetail = new OrderDetail({
//                 product: product,
//                 unitPrice: unitPrice,
//                 quantity: quantity,
//                 order: order
//             })
//             newOrderDetail.save()
//                 .then(orderDetail => {
//                     console.log(orderDetail)
//                     return orderDetail
//                 })
//                 // .catch(err => console.log(err))
//         })
//         promises.push(p)
//     })
//     Promise.all(promises)
//         .then(result => {
//             console.log(result)
//             res.status(200).json(result)
//         })
//         .catch(err => res.status(400).json({ msg: 'Hệ thống gặp lỗi khi lưu thông tin', err }))

// })



router.put("/update/:id", (req, res) => {
    OrderDetail.findById(req.params.id, (err, orderDetail) => {

        if (req.body.productDetailName !== undefined)
            orderDetail.productDetailName = req.body.productDetailName

        if (req.body.birthday !== undefined)
            orderDetail.birthday = req.body.birthday

        if (req.body.phone !== undefined)
            orderDetail.phone = req.body.phone

        if (req.body.address !== undefined)
            orderDetail.address = req.body.address

        if (req.body.address !== undefined)
            orderDetail.address = req.body.address

        orderDetail.save()
            .then(orderDetail => {
                res.json('object updated successfully: ' + orderDetail);
            })
            .catch(err => {
                res.status(400).send("unable to update data: " + err);
            });
    });
});

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    OrderDetail.findById(req.params.id, (err, orderDetail) => {

        orderDetail.isDeleted = true; //thay doi isDeleted

        orderDetail.save()
            .then(() => {
                res.json('object deleted successfull');
            })
            .catch(err => {
                res.status(400).send("error: " + err);
            });
    })
});

module.exports = router;