const express = require('express')
const router = express.Router()

const Product = require('../models/product-model')
const OrderDetail = require('../models/orderDetail-model')

//get product by id and list orderDetail embed
router.get("/:id", (req, res) => {

    let arrOrderDetail = []
    // console.log(req.params.id)
    Product.findById(req.params.id, (err, product) => {
        if (err) throw err
        //return res.status(200).json({ product })
        const orderDetails = product.get('orderDetails')
        if (orderDetails.length > 0)
            return orderDetails.map(id => {

                return OrderDetail.findById(id, (err, orderDetail) => {

                    if (err) throw err
                    arrOrderDetail = [...arrOrderDetail, orderDetail]

                })
                    .then(() => {
                        return res.status(200).json(product)
                        // return res.status(200).json({ product, arrOrderDetail })
                    })
            })
        else
            return res.status(200).json(product)
    })
})

router.get("/", (req, res) => {
    Product.find({}, (err, products) => {
        if (!err)
            res.status(200).json(products)
        else {
            res.status(400).json({ msg: 'loi' })
        }
    })
})

// router.post('/filter', (req, res) => {
//     const { filter } = req.body
//     if (filter !== '')
//         Product.find({ productName: filter, isDeleted: false }, (err, products) => {
//             if (!err)
//                 res.status(200).json(products)
//             else {
//                 res.status(400).json({ msg: 'loi' })
//             }
//         })
//     else
//         Product.find({ isDeleted: false }, (err, products) => {
//             if (!err)
//                 res.status(200).json(products)
//             else {
//                 res.status(400).json({ msg: 'loi' })
//             }
//         })
// })

router.post('/getproducts', (req, res) => {
    Product.find({ isDeleted: false })
        .then(products => {
            res.status(200).json(products)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Không tìm được dữ liệu', err })
        })
})

//create driver
router.post('/create', (req, res) => {
    const product = new Product(req.body)
    product.save()
        .then(
            res.status(200).json(product)
        )
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi khi lưu thông tin' })
        })
})

//update Product
router.put("/update/:id", (req, res) => {

    const newData = {
        productName: req.body.productName,
        price: req.body.price,
        urlImage: req.body.urlImage
    }

    Product.findByIdAndUpdate(req.params.id, newData)
        .then(product => {
            res.status(200).json(product)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Cập nhật thất bại !' })
        })
});

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    Product.findById(req.params.id, (err, product) => {

        product.isDeleted = true; //thay doi isDeleted

        product.save()
            .then(() => {
                res.json('object deleted successfull');
            })
            .catch(err => {
                res.status(400).send("error: " + err);
            });
    })
});

module.exports = router;