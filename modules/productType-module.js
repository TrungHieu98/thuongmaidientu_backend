const express = require('express')
const router = express.Router()

const ProductType = require('../models/productType-model')
const Product = require('../models/order-model')

//get productType by id and list order embed
router.get("/:id", (req, res) => {
    let arrProduct = []
    // console.log(req.params.id)
    ProductType.findById(req.params.id, (err, productType) => {
        if (err) throw err
        //return res.status(200).json({ productType })
        const products = productType.get('products')
        if (products.length > 0)
            return products.map(id => {

                return Product.findById(id, (err, order) => {

                    if (err) throw err
                    arrProduct = [...arrProduct, order]

                })
                    .then(() => {
                        // return res.status(200).json({ productType, arrProduct })
                        return res.status(200).json(productType)

                    })
            })
        else
            return res.status(200).json(productType)
    })
})

router.get("/", (req, res) => {
    ProductType.find({}, (err, productType) => {
        if (err)
            res.status(400).json({ msg: 'Không tìm được dữ liệu' })
        res.status(200).json(productType)
    })
})

router.post('/create', (req, res) => {
    const productType = new ProductType(req.body)
    //console.log("req.body:", req.body)
    productType.save()
        .then(
            res.status(200).json(productType)
        )
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi khi lưu thông tin' })
        })
}
)

//update ProductType
router.put("/update/:id", (req, res) => {

    const newData = {
        typeName: req.body.typeName
    }

    ProductType.findByIdAndUpdate(req.params.id, newData)
        .then(producttype => {
            res.status(200).json(producttype)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Cập nhật thất bại !' })
        })
});

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    ProductType.findByIdAndUpdate(req.params.id, { $set: { isDeleted: true } })
        .then(productType => {
            console.log('from productType module: ', req.params.id)
            res.status(200).json({ msg: 'Đã xóa !' })
        })
        .catch(err => {
            res.status(400).json({ msg: 'Xóa thất bại !' })
        })
});

router.put("/restore/:id", (req, res) => {
    ProductType.findByIdAndUpdate(req.params.id, { $set: { isDeleted: false } })
        .then(productType => {
            res.status(200).json({ msg: 'Đã khoi phuc !' })
        })
        .catch(err => {
            res.status(400).json({ msg: 'khoi phuc thất bại !' })
        })
})

router.post('/getproducttypes', (req, res) => {
    ProductType.find({ isDeleted: false })
        .then(productTypes => {
            res.status(200).json(productTypes)
        })
        .catch(err => {
            res.status(400).json({msg: 'Không tìm được dữ liệu', err})
        })
})

module.exports = router;