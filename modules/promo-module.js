const express = require('express')
const router = express.Router()

const Promo = require('../models/promo-model')
const Order = require('../models/order-model')
const User = require('../models/user-model')

//get promo by id and list order embed
router.get("/:id", (req, res) => {
    let arrOrder = []
    Promo.findById(req.params.id, (err, promo) => {
        if (err)
            res.status(400).json({ msg: 'Không tìm thấy Promo' })

        const orders = promo.get('orders') //lấy mảng orders từ promo

        // if (orders.length > 0)
        //     orders.map(id => {

        //         return Order.findById(id, (err, order) => {

        //             if (err) throw res.status(400).json({ msg: 'Lỗi tìm kiếm thông tin' })
        //             arrOrder = [...arrOrder, order]

        //         })
        //             .then(() => {
        //                 // res.status(200).json({ promo, arrOrder })
        //                 res.status(200).json(promo)
        //             })
        //     })
        // else
        res.status(200).json(promo)
    })
})

//get promo dang ton tai
router.get('/get/list', (req, res) => {
    Promo.find({ isDeleted: false })
        .then(existedPromos => {
            const promos = existedPromos.filter(promo => promo.quantity > 0)
            res.status(200).json(promos)
        })
        .catch(err => res.status(400).json({ msg: 'Không tìm thấy bất kì mã khuyễn mãi nào!' }))
})

//get all promo
router.get('/', (req, res) => {
    Promo.find({})
        .then(promos => res.status(200).json(promos))
        .catch(err => res.status(400).json({ msg: 'Không tìm thấy bất kì khuyến mãi nào!', err }))
})

//create promo
router.post('/create', (req, res) => {
    const promo = new Promo({
        promoName: req.body.promoName,
        percent: req.body.percent,
        maxDiscount: req.body.maxDiscount,
        minOrderPrice: req.body.minOrderPrice,
        description: req.body.description,
        quantity: req.body.quantity,
        producer: req.body.producer,
        expiredDate: req.body.expiredDate,
        urlImage: req.body.urlImage
    })

    promo.save()
        .then(
            res.status(200).json(promo)
        )
        .catch(err => {
            res.status(400).json({ msg: 'Lưu thất bại !' })
        })
})

//update promo
router.put("/update/:id", (req, res) => {

    const newData = {
        promoName: req.body.promoName,
        percent: req.body.percent,
        maxDiscount: req.body.maxDiscount,
        minOrderPrice: req.body.minOrderPrice,
        description: req.body.description,
        quantity: req.body.quantity,
        producer: req.body.producer,
        expiredDate: req.body.expiredDate,
        urlImage: req.body.urlImage,
    }

    Promo.findByIdAndUpdate(req.params.id, newData)
        .then(promo => {
            res.status(200).json(promo)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Cập nhật thất bại !' })
        })
});

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    Promo.findByIdAndUpdate(req.params.id, { $set: { isDeleted: true } })
        .then(promo => {
            console.log('from promo module: ', req.params.id)
            res.status(200).json({ msg: 'Đã xóa !' })
        })
        .catch(err => {
            res.status(400).json({ msg: 'Xóa thất bại !' })
        })
})

router.post('/getpromos', async (req, res) => {
    const { promos } = req.body
    const arr = await promos.map(promo_id => {
        return Promo.findById(promo_id)
    })
    Promise.all(arr)
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau' })
        })
})

router.put("/restore", (req, res) => {
    Promo.find({ isDeleted: true })
        .then(res => {
            res.status(200).json('')
        })

    // .then(res.map(
    //     promo => {
    //         promo.isDeleted = false
    //         promo.save()
    //     }
    // ))
    // .then(
    //     res.status(200).json(res)
    // )
    // .catch(err => {
    //     res.status(400).json({ msg: 'loi khoi phuc' })
    // })
});

router.post('/takepromo', (req, res) => {
    const { promo_id, user_id } = req.body
    Promo.findById(promo_id)
        .then(promo => {
            if (promo.users.indexOf(user_id) === -1) {
                promo.users = [user_id, ...promo.users]
                promo.quantity = promo.quantity - 1
                promo.save()

                User.findById(user_id)
                    .then(user => {
                        user.promos = [promo_id, ...user.promos]
                        user.save()
                            .then(user => res.status(200).json({ user, promo }))
                            .catch(err => res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau!' }))
                    })
            }
            else
                res.status(400).json({ msg: 'Bạn đã nhận khuyến mãi này trước đây!' })
        })
        .catch(err => res.status(400).json({ msg: 'Khuyễn mãi không tồn tại!' }))
})

module.exports = router;