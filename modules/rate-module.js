const express = require('express')
const router = express.Router()

const Rate = require('../models/rate-model')
const Product = require('../models/product-model')

//get rate by id and list product embed
router.get("/:id", (req, res) => {

    let arrProduct = []
    // console.log(req.params.id)
    Rate.findById(req.params.id, (err, rate) => {
        if (err || rate.isDeleted) throw err
        //return res.status(200).json({ rate })
        const products = rate.get('products')
        if (products.length > 0)
            return products.map(id => {

                return Product.findById(id, (err, product) => {

                    if (err) throw err
                    arrProduct = [...arrProduct, product]

                })
                    .then(() => {
                        return res.status(200).json({ rate, arrProduct })
                    })
            })
        else
            return res.status(200).json({ rate })
    })
})

router.get('/', (req, res) => {
    Rate.find({}, (err, rate) => {
        if (err || rate.isDeleted) throw err
        return res.json(rate)
    })
})

//create driver
router.post('/create', (req, res) => {
    const rate = new Rate(req.body)
    rate.save()
        .then(rate => {
            res.json('object added successfully: ' + rate)
        })
        .catch(err => {
            res.status(400).send("unable to save to database: " + err)
        })
})

//update driver
router.put("/update/:id", (req, res) => {
    Rate.findById(req.params.id, (err, rate) => {

        if (req.body.star !== undefined)
            rate.star = req.body.star

        if (req.body.comment !== undefined)
            rate.comment = req.body.comment

        rate.save()
            .then(rate => {
                res.json('object updated successfully: ' + rate)
            })
            .catch(err => {
                res.status(400).send("unable to update data: " + err)
            })
    })
})

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    Rate.findById(req.params.id, (err, rate) => {

        rate.isDeleted = true; //thay doi isDeleted

        rate.save()
            .then(() => {
                res.json('object deleted successfull');
            })
            .catch(err => {
                res.status(400).send("error: " + err);
            });
    })
});

module.exports = router;