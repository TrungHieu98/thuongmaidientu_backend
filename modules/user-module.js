const express = require('express')
const router = express.Router()
const User = require('../models/user-model')
const Order = require('../models/order-model')
const Promo = require('../models/promo-model')

//get user by id and list order embed
router.get("/:id", (req, res) => {

    let arrOrder = []
    // console.log(req.params.id)
    User.findById(req.params.id, (err, user) => {
        if (err || user.isDeleted) throw err
        //return res.status(200).json({ user })
        const orders = user.get('orders')
        // if (orders.length > 0)
        //     return orders.map(id => {

        //         return Order.findById(id, (err, order) => {

        //             if (err) throw err
        //             arrOrder = [...arrOrder, order]

        //         })
        //             .then(() => {
        //                 return res.status(200).json({ user, arrOrder })
        //             })
        //     })
        // else
        return res.status(200).json(user)
    })
})

//get all list
router.get("/", (req, res) => {
    User.find({ isDeleted: false }, (err, users) => {
        if (err)
            res.status(400).json({ msg: 'Không tìm được dữ liệu' })
        res.status(200).json(users)
    })
})

router.post('/auth/login', (req, res) => {
    // kiem tra email da ton tai
    // console.log(req.body)
    User.findOne({ email: req.body.email })
        .then(user => {
            if (user !== null) {
                if (req.body.password === user.password)
                    res.status(200).json(user)
                else
                    res.status(400).json({ msg: 'Bạn đã nhập sai mật khẩu !' })
            }
            else {
                res.status(400).json({ msg: 'Người dùng không tồn tại !' })
            }
        })
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi khi đăng nhập' })
        })
})

//loadser
router.post('/auth/load', (req, res) => {
    User.findById(req.body.userId)
        .select('-password')
        .then(user => {
            res.status(200).json(user)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Không load được user' })
        })
})

//create user = register
router.post('/auth/register', (req, res) => {
    // kiem tra email da ton tai
    User.findOne({ email: req.body.email })
        .then(user => {
            if (user !== null) {
                // console.log('user: ', user)
                res.status(400).json({ msg: 'Email này đã được sử dụng !' })
            }
            else {
                const user = new User(req.body)
                //console.log("req.body:", req.body)
                user.save()
                    .then(
                        res.status(200).json(user)
                    )
                    .catch(err => {
                        res.status(400).json({ msg: 'Hệ thống gặp lỗi khi lưu thông tin' })
                    })
            }
        })
        .catch(err => {
            res.status(400).json({ msg: 'Hệ thống gặp lỗi khi đăng kí' })
        })
})


// router.get('/get/email', (req, res) => {
//     console.log(req.body.email)
//     User.findOne({ email: req.body.email })
//         .then((user, err) => {
//             if (!user) throw err;
//             return res.status.json(user)
//         })
// })


//update user
router.put("/update/:id", (req, res) => {
    const newData = {
        userName: req.body.userName,
        birthday: req.body.birthday,
        phone: req.body.phone,
        address: req.body.address,
        password: req.body.password,
        priority: req.body.priority,
    }
    User.findByIdAndUpdate(req.params.id, newData)
        .then(user => {
            res.status(200).json(user)
        })
        .catch(err => {
            res.status(400).json({ msg: 'Cập nhật thất bại !' })
        })
});

//delete (ko xoa that su)
router.put("/delete/:id", (req, res) => {
    User.findById(req.params.id, (err, user) => {
        user.isDeleted = true; //thay doi isDeleted
        user.save()
            .then(() => {
                res.json('object deleted successfull');
            })
            .catch(err => {
                res.status(400).send("error: " + err);
            });
    })
});

// router.put('/takepromo', (req, res) => {
//     const { user_id, promo_id } = req.body
//     Promo.findById(promo_id)
//         .then(promo => {
//             promo.quantity = promo.quantity - 1
//             promo.save()
//                 .then(
//                     User.findById(user_id)
//                         .then(user => {
//                             res.status(200).json({ user, promo })
//                         })
//                         .catch(err => res.status(400).json(err))
//                 )
//         })
//         .catch(err => res.status(400).json(err))

// User.findByIdAndUpdate(user_id, newData)
//     .then(user => {
//         res.status(200).json(user)
//     })
//     .catch(err => {
//         res.status(400).json(err)
//     })
// })

router.put('/usepromo', (req, res) => {
    const { user, promo_id } = req.body
    User.findById(user._id, (err, user) => {
        const index = user.promos.indexOf(promo_id)
        user.promos.splice(index, 1)
        user.save()
            .then(user => {
                res.status(200).json(user)
            })
            .catch(err => {
                res.status(400).json(err)
            });
    })
})

router.post('/recordorder', (req, res) => {
    const { order_id, user_id } = req.body
    User.findById(user_id)
        .then(user => {
            user.orders = [order_id, ...user.orders]
            user.save()
                .then(res.status(200).json(user))
                .catch(err => res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau!', err }))
        })
        .catch(err => res.status(400).json({ msg: 'Người dùng không tồn tại!', err }))
})

router.post('/get/orders', (req, res) => {
    const { user_id } = req.body
    User.findById(user_id)
        .then(async user => {
            const arrOrder = await user.orders.map(async order_id => {
                return Order.findById(order_id)
            })
            Promise.all(arrOrder)
                .then(orders =>
                    res.status(200).json(orders)
                )
                .catch(err => res.status(400).json({ msg: 'Hệ thống gặp lỗi, vui lòng thử lại sau!', err }))
        })
        .catch(err => res.status(400).json({ msg: 'Người dùng không tồn tại!', err }))
})

module.exports = router;