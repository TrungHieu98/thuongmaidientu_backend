require('dotenv').config();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 9000;
const cors = require('cors');
const mongoose = require('mongoose')


//modules
const userRouter = require('./modules/user-module')
const orderRouter = require('./modules/order-module')
const orderDetailRouter = require('./modules/orderDetail-module')
const productRouter = require('./modules/product-module')
const productTypeRouter = require('./modules/productType-module')
const promoRouter = require('./modules/promo-module')
const rateRouter = require('./modules/rate-module')
const adminRouter = require('./modules/admin-module')
const cartRouter = require('./modules/cart-module')

//connect database
// connect DB
mongoose.connect(process.env.DB_LOCAL || process.env.DB_HOST,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
)
  .then(() => console.log('connected'))
  .catch(err => console.log(err));

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//use router
app.use('/api/user', userRouter)
app.use('/api/order', orderRouter)
app.use('/api/orderdetail', orderDetailRouter)
app.use('/api/product', productRouter)
app.use('/api/producttype', productTypeRouter)
app.use('/api/promo', promoRouter)
app.use('/api/rate', rateRouter)
app.use('/api/admin', adminRouter)
app.use('/api/cart', cartRouter)

app.get('/', function (req, res) {
  res.json('welcome to thuong mai dien tu backend')
})

app.listen(process.env.PORT || process.env.PORT_LOCAL, function () {
  console.log('Server is running on Port:', process.env.PORT || process.env.PORT_LOCAL);
})

module.exports = app

//Ban cu~

// const express = require('express');
// const app = express();
// const bodyParser = require('body-parser');
// const PORT = 9000;
// const cors = require('cors');

// //modules
// const userRouter = require('./modules/user-module')
// const orderRouter = require('./modules/order-module')
// const orderDetailRouter = require('./modules/orderDetail-module')
// const productRouter = require('./modules/product-module')
// const productTypeRouter = require('./modules/productType-module')
// const promoRouter = require('./modules/promo-module')
// const rateRouter = require('./modules/rate-module')
// const adminRouter = require('./modules/admin-module')
// const cartRouter = require('./modules/cart-module')

// //connect database
// require('./db');

// app.use(cors());
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());

// //use router
// app.use('/api/user', userRouter)
// app.use('/api/order', orderRouter)
// app.use('/api/orderdetail', orderDetailRouter)
// app.use('/api/product', productRouter)
// app.use('/api/producttype', productTypeRouter)
// app.use('/api/promo', promoRouter)
// app.use('/api/rate', rateRouter)
// app.use('/api/admin', adminRouter)
// app.use('/api/cart', cartRouter)

// app.listen(PORT, function () {
//   console.log('Server is running on Port:', PORT)
// })
